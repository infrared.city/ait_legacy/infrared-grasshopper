# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import System
import Rhino
import scriptcontext as sc
import rhinoscriptsyntax as rs
import json

import InFraReDUtils as utils


class RunAnalyses(component):
    
    def __init__(self):
        super(RunAnalyses, self).__init__()
        
        # Defaults (None indicates required input params)
        self.N = None
    
    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Run Analyses',
            'IR Run',
            "Use this component to add your project to the InFraReD's Analysis Engine and return the results.",
            'InFraReD',
            '5 | Run'
        )
        return instance
    
    def get_ComponentGuid(self):
        return System.Guid('4e6135f2-28d4-40f7-835a-f491e536f70d')
    
    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional
    
    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Name', 'N', 'The name of your snapshot.\nIf the name already exists, the corresponding data will be overwritten.\n If no snapshot with the name exists, a new one will be created.')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)
        
    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Analyses Results', 'A', '')
        self.Params.Output.Add(p)
        
    def SolveInstance(self, DA):
        N = self.marshal.GetInput(DA, 0)
        result = self.RunScript(N)
        if result is not None:
            self.marshal.SetOutput(result, DA, 0, True)
        
    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACQ0lEQVRIie2UT2gTURDGv2lsYkOhaaK0HsS0YkVKMbaHItJ0ayi2QrCHHkR6WMEtgodG6D3Gkz2l4klyyIqCgpciiB4Uo4hWD+uWFjG1YqiiDVQbDyX/CCO7dEMSg5uKnswH77DDvN+8+ea9RV3/pyx/0jWJkps8fYCqZGrJ31YRHS4cCrtGFmVrznI6u8+7AFVJ/JXx6PDAuSjNnWLa3MWdDB5n8NBcH5MohU33m8Hh2BmE0Cry8DfAvgjCAjrwA72AvppXXbhx65iqvG87y3JErcapapd+cu/xMEYEmSf7PejJA41JAEkQkmhFFnsAfXW3pDE9EG/Pf7edf+UcJahKrJK345eTN7YE0ekV+aQP6CbAsmTmhq7wiZcYsySCF2zjg2+zrVpXxVk1FE8+MR1Fzv+R20Ii7/cDNntN8FINOr/iefs9YbLw7g2JUqCsCAABG4fHOOMDaHvwy9eGcen2UaTSVv3bQTmM5j45nIXslO6MYRfLEZlExIhXopw/IwBdNReZV9yYB3DnaRem3Et4vb478aBpb0hjGjnFmWx5OETilwDivjAf9JtdvjKlslaE4r1GqOztNFQmsxyZxYfHR+jmFRXLy78Ff151VQtrFj0hUXIYgeovXlXW8OLZddosEKUzAnqs+vU1rnDzuhVrd/tTj+SBGX2eFU0BmGE58tAImPpBouTBgaYoT+Q8sK+A7iOFWMdVALMsR1IkSlySrs3hohYvY5gVKSlm/D5CpRASpQ2t9y141RdfV13/SAB+Al2SzMGokujaAAAAAElFTkSuQmCC'
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )
    
    def RunScript(self, N):
        result = None
        
        # Validate authentication
        auth_token = sc.sticky.get('InFraReDAuthToken')
        if not auth_token:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                "Invalid Credentials.\nPlease authenticate with the InFraReD server using the 'InFraReD Authenticate' component."
            )
            return
        
        # Grab stickies
        client_uuid = sc.sticky.get('InFraReDClientUuid')
        
        # Input parameter checking and defaults
        if N is not None:
            self.N = N
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter N failed to collect data.'
            )
            return
            
        # Get project uuid
        project_uuid = sc.sticky.get('InFraReDProjectUuid')
        if not project_uuid:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                "No Project Found.\nPlease create a project using the 'InFraReD Create Project' component or load one using the 'InFraReD Load Project' component."
            )
            return
        
        # Get snapshot list from server 
        gql = System.Text.RegularExpressions.Regex.Replace("""
            {{ \" query \":
                \" query {{
                    getSnapshotsByProjectUuid (
                        uuid: \\\" {} \\\"
                    ) {{
                        success,
                        infraredSchema
                    }}
                }} \"
            }}
            """,
            "\s+",
            ""
        ).format(
            project_uuid
        )
        
        # Send gql req
        out, auth_token = utils.send_gql_req_to_api(gql, auth_token)
        
        # Store server-created project uuid in a sticky
        return_data = json.loads(out)['data']['getSnapshotsByProjectUuid']
        if return_data['success'] == True:
            snapshots = return_data['infraredSchema']['clients'][client_uuid]['projects'][project_uuid]['snapshots']

        # Make sure root snap is in snapshost
        snapshot_uuid = utils.match_snaps(snapshots, self.N)
        if not snapshot_uuid:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                "Invalid snapshot name (N).\nNo snapshots found with that exact name. Please check that the Name (N) can be found in the Snapshots (s) output from either the 'InFraReD Create Project' or 'InFraReD Load Project' components."
            )
            return
        
        # GQL Run services
        gql = System.Text.RegularExpressions.Regex.Replace("""
            {{ \" query \":
                \" mutation {{
                    runServicesGrasshopper (
                        snapshotUuid : \\\" {} \\\"
                    ) {{
                        success,
                        infraredSchema
                    }}
                }} \"
            }}
            """,
            "\s+",
            ""
        ).format(
            snapshot_uuid
        )
        
        # Send gql req
        out, auth_token = utils.send_gql_req_to_api(gql, auth_token)
        
        # Store server-created project uuid in a sticky
        return_data = json.loads(out)['data']['runServicesGrasshopper']
        if return_data['success'] == True:
            result = Grasshopper.DataTree[object]()
            for pth, analysis in enumerate(return_data['infraredSchema'].values()):
                result.Add(analysis, Grasshopper.Kernel.Data.GH_Path(pth))
        
        return result

class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):
    def get_AssemblyName(self):
        return 'RunAnalyses'
    
    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return 'City Intelligence Lab'

    def get_AuthorName(self):
        return ''
    
    def get_Id(self):
        return System.Guid('5c060304-0538-4158-975e-2711a9fa878b')