# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import System

import InFraReDUtils as utils

class SolarRadiationAnalysisContainer(utils.Analysis):
    def __repr__(self):
        return '<InFraReD.SolarRadiationAnalysis instance at {:#018x}>'.format(
            id(self)
        )

class SolarRadiationAnalysis(component):

    def __init__(self):
        super(SolarRadiationAnalysis, self).__init__()

        # Defaults (None indicates required input params)
        self.v = {}
        self.f = {}

    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Solar Radiation Analysis',
            'IR Solar Radiation',
            "One of InFraReD's analysis modules. Merge multiple of these analysis components to define all the analyses ran on your project.\n\nThe solar radiation analysis module leverages a deep learning model and was trained on data from Vienna, Austria.",
            'InFraReD',
            '1 | Analysis'
        )
        return instance

    def get_ComponentGuid(self):
        return System.Guid("809db8da-e9a3-4e52-b0c3-7b3d6199c2f1")

    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional

    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, "Visualisation", "v", "Currently not functioning. This input parameter is safe to leave empty.", True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, "Filters", "f", "Currently not functioning. This input parameter is safe to leave empty.", True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Analysis Container', 'A', 'A container for this analysis and the arguments.')
        self.Params.Output.Add(p)

    def SolveInstance(self, DA):
        v = self.marshal.GetInput(DA, 0)
        f = self.marshal.GetInput(DA, 1)

        result = self.RunScript(v, f)
        if result is not None:
            self.marshal.SetOutput(result, DA, 0, True)

    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAABQklEQVRIie1V3U3DMBD+jBggbNARGKGZADbg+uBnyAYwQcuzHzgmACZoR+gGMEI2OHTSRXItu7EReesnRYp99vfdT+6CCxaDI79y5J9a+a8az68AbJcW+ROyIo782pEXR54y5jFz/s2R/26N5GiPXr6fNoXDQTjcJAKaPnXmUBJxRQP5TkUAfAH4BKAFv7O6qAPvwoE1Co1OOAzNIonYHsBtxszCYTPHUVP4bUFAQYW61YtYFHMkD3Mi1wmppmVtS63D6xxBdFei5SAcdlkRNQLo7P2nVsDQR+/HEwcqPIyjy+FFODyf46gp/JBrwMjjXcFWJ2LDsLNUpM3Gtk9xwzbBRoXEU1e/Nh05iSMfZ0ZQORIjImu2OB3a7Y/J8c00gppE7FKf6WZN3UlqhMNoaevxH5imcytV6/9ktCa9YCEA+AVvy2CGNS6RIwAAAABJRU5ErkJggg=='
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )

    def RunScript(self, v, f):
        result = None
        
        # Input parameter checking and defaults
        if v is not None:
            # if TODO: validation
            #     component.AddRuntimeMessage(...
            # else:
            #     self.v = v
            pass
        if f is not None:
            # if TODO: validation
            #     component.AddRuntimeMessage(...
            # else:
            #     self.f = f
            pass
            
        # Compose session settings analysis
        result = SolarRadiationAnalysisContainer(
            {
                "name": "Solar Radiation",
                "parameters": []
            }
        )
        
        return result


class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):

    def get_AssemblyName(self):
        return 'SolarRadiationAnalysis'

    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return '0.1'

    def get_AuthorName(self):
        return 'City Intelligence Lab'

    def get_Id(self):
        return System.Guid('cba02d4f-4bbe-4e10-81b0-4ccc99fc24e8')