# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import System

import InFraReDUtils as utils

class WindSpeedAnalysisContainer(utils.Analysis):
    def __repr__(self):
        return '<InFraReD.WindSpeedAnalysis instance at {:#018x}>'.format(
            id(self)
        )

class WindSpeedAnalysis(component):

    def __init__(self):
        super(WindSpeedAnalysis, self).__init__()

        # Defaults (None indicates required input params)
        self.S = 10
        self.D = 0
        self.v = {}
        self.f = {}

    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Wind Speed Analysis',
            'IR Wind Speed',
            "One of InFraReD's analysis modules. Merge multiple of these analysis components to define all the analyses ran on your project.",
            'InFraReD',
            '1 | Analysis'
        )
        return instance

    def get_ComponentGuid(self):
        return System.Guid("52618a03-3d75-4849-86b4-e14dc782ce37")

    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional

    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_Number()
        self.SetUpParam(p, 'Wind Speed', 'S', 'Wind speed (m²) input argument for analysis module.', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_Number()
        self.SetUpParam(p, "Wind Direction", "D", "Wind direction (°) input argument for analysis module.", True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, "Visualisation", "v", "Currently not functioning. This input parameter is safe to leave empty.", True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, "Filters", "f", "Currently not functioning. This input parameter is safe to leave empty.", True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Analysis Container', 'A', 'A container for this analysis and the arguments.')
        self.Params.Output.Add(p)

    def SolveInstance(self, DA):
        S = self.marshal.GetInput(DA, 0)
        D = self.marshal.GetInput(DA, 1)
        v = self.marshal.GetInput(DA, 2)
        f = self.marshal.GetInput(DA, 3)

        result = self.RunScript(S, D, v, f)
        if result is not None:
            self.marshal.SetOutput(result, DA, 0, True)

    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAABk0lEQVRIie2Uz0oCURTGPyOQVjNYaCXiLGpnaSC6CGIyENpFvcC4cB0+QfQEPcAsml6gPyuhRQ5BCyNIw03MxjGNlBDdpK4mznBHSkc0kmgxH1zOZc6953fOuYeBo38n16iEXFKaB3AiRmOS+nBfBlAAUGS2YChyedJiZgcCRwBYl3OBxSVhP5FErVEXtIou+L2+vYOdJKr1OmqhMPKlosqgOgOrYyEAHgFkABwCOF5e8J7Sx5VAEFpFJ5i5p0XSXnSx2W6J8VAY8bV1VBNJnN9cU5IZQ5EvraAzNuAjAClDkRUPxw05CWRpnvmrjTcTLEZj8HC8ACDy9Y4dhDJp2TZ3hDq9Xt8x53aPbReJ+nrhktKpZrs95PR7ff39R7dr2tVAEHSWKmKVFsZBrgCcEej1vUEV8VaLPByPTq9rBnrSnsmSv5AvFWnR5JXtHv/bCLuktAFgmw6yEc7tbm5Fsne3VnZlNsYqCzjRGNtVYspQZMpyIwuIo0ZzUtk9/CDsVwBMApmGHIgD+TvIj36MjqYvAJ+toJnTYB0a6wAAAABJRU5ErkJggg=='
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )

    def RunScript(self, S, D, v, f):
        result = None
        
        # Input parameter checking and defaults
        if S is not None:
            if S < 1 or S > 25:
                component.AddRuntimeMessage(
                    self,
                    Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                    'Invalid wind speed (S).\nPlease enter a valid wind speed, between 1 and 25 ms.\nOr leave this parameter empty to have it default to 10m/s.'
                )
                return
            else:
                self.S = S
        if D is not None:
            self.D = D % 360
        if v is not None:
            # if TODO: validation
            #     component.AddRuntimeMessage(...
            # else:
            #     self.v = v
            pass
        if f is not None:
            # if TODO: validation
            #     component.AddRuntimeMessage(...
            # else:
            #     self.f = f
            pass
            
        # Compose session settings analysis
        result = WindSpeedAnalysisContainer(
            {
                "name": "Wind Speed",
                "parameters": [
                    {
                        "name": "Wind Speed",
                        "id": "windSpeed",
                        "tag": "slider",
                        "value": str(self.S)
                    },
                    {
                        "name": "Wind Direction",
                        "id": "windDirection",
                        "tag": "slider",
                        "value": str(self.D)
                    }
                ]
            }
        )
        
        return result


class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):

    def get_AssemblyName(self):
        return 'WindSpeedAnalysis'

    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return '0.1'

    def get_AuthorName(self):
        return 'City Intelligence Lab'

    def get_Id(self):
        return System.Guid('cf824af1-952b-4a8f-a7ec-7cf65286a860')