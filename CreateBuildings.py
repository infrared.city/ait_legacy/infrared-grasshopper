# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import System
import rhinoscriptsyntax as rs
import ghpythonlib.components as ghcomp
import ghpythonlib.parallel as pr

import InFraReDUtils as utils

class CreateBuildings(component):

    def __init__(self):
        super(CreateBuildings, self).__init__()

        # Defaults (None indicates required input params)
        self.B = None
        self.U = ['residential']
        
    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Create Buildings',
            'IR Create Buildings',
            'Use this component to turn meshes into InFraReD buildings.',
            'InFraReD',
            '3 | Geometry'
        )
        return instance

    def get_ComponentGuid(self):
        return System.Guid('ec1e0d8f-0d41-4cd4-aad7-072961b23b36')
    
    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional
    
    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_Mesh()
        self.SetUpParam(p, 'Buildings', 'B', 'A list of meshes representing buildings.')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.list
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Use', 'U', 'A list of land uses corresponding to the list of buildings.', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.list
        self.Params.Input.Add(p)
    
    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Buildings', 'B', 'Buildings in the InFraReD format. Used as input in the "InFraReD Create Snapshot" component.')
        self.Params.Output.Add(p)
    
    def SolveInstance(self, DA):
        B = self.marshal.GetInput(DA, 0)
        U = self.marshal.GetInput(DA, 1)
        result = self.RunScript(B, U)

        if result is not None:
            self.marshal.SetOutput(result, DA, 0, True)
        
    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAADQElEQVRIid1V30tTcRQ/1xmbbugm4tJpu5ZLE9HZHowGa0ZBe8gfRQ89SFfiEj1o1h/QJvWeghAyYfelRy0pCgp1ChmYbdN0pXM2XaNWaHe66YY/Ft/rvWvujpnokx+4XL7nfs/5fD/nnO+5cPSBESR+WCIFiQaMIPW4oW6oDD9p8hUoMEytcYDDFj4ICRYXHJdkZlrq9Zf0NRWVjM33yw8vRwY9znl3e5QyU6kCnX74WF9SpLwFAO2vySYPjwQjyAZFnvx5680myBCKeAGs42PQN/iWilLm5iTK8eb665YyvFiPfL0/f9BTblenQavraClX0mhPGrtXbdDqoG/gHcx5F2IB1iNhFBzs0xMgk4h5NcIIss10p+VbdekZPXe4ouP5UoNWZxz+9NHO1TWdc6hUlTLPm/cjMOmagZwsKYxPOaAwRwb5Miksra7iGEGakLAoZbYinxuXr9S7vAtQGJGDIk8eO5hzbhb8fh8iQI8nPfF0SBHa2PWMAlVBPvxeWYFjIjE8aLqNZ4iExknXrPGVRELRXU+a5xYXILi+BstFSsYnEFyFRZ8XHLNfd8XkkSAg6emCncbbBAGQdddi31BTZAiFRE8wOMzZUIpR3dQqFXyY+szYZBJxzIerCY1SlAg6FAJttYZnR2nNlkguxNuQks2trWRn3lESpcwdPQB0+clTxqu6iziX38NCLF3oHjgJ8oVz3t12vursPToUlPqWlqzhKIZXqkp3dRZKTWAnXf81FdLiF1HKTEcps2l0wlb9JxiqjfQ8rXXOuxst/b00yvtyIMAQDIyNUomXM9n92pWuREQpswcjSI7YYSfIYvvMlwYAkLIt7Ej0QTXZFwl7iQgAMHEKAYA3ViSZYqaF90Jasu9IyZ6e+0BSEhZKjCClrDIpGiHcmkPJCSV89/thLZx6SKci6QQAS8J/Bc0jS/wmVAs0rWsqqvZPwhYXTV1UcCNbdA/75iEnOztmQtOiMDf33zqVTLbgHdwaI8ghALjPrS39ve0sqZoLXiyXMyNlLRJBjcJ0Ie/PmAqYWoNxE5iBw+YBh60bU2sCeIHinAAD0cZGxLq1vd04/cjYfdA/Kg+oKbLutqLUHlUAwF+aMEkH0esHZQAAAABJRU5ErkJggg=='
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )

    def mesh2json(self, master):
        """Definition to process meshes."""

        # unpack master list
        mesh = rs.coercemesh(master['mesh'])
        buildingUse = master['buildingUse']
        refX = master['projOrigin'][0]
        refY =  master['projOrigin'][1]
        
        # get footprint
        vec = rs.CreateVector([0, 0, 1])
        plane = rs.CreatePlane([0, 0, 0])
        curves = []
        crv = ghcomp.MeshShadow(mesh, vec, plane)
        if crv == None:
            # smooth mesh in cased mesh shadow failed
            smooth_m = ghcomp.SmoothMesh(mesh, 0.5, True, 1)
            crv = ghcomp.MeshShadow(smooth_m, vec, plane)
        
        # simplify curves
        curves = ghcomp.SimplifyCurve(crv).curve
        
        # get height
        verts = ghcomp.DeconstructMesh(mesh).vertices
        z = []
        for i in verts:
            z.append(i.Z)
        z.sort()
        min_, max_ = z[0], z[-1]
        height = round(max_ - min_, 2)
        
        # create JSON structure
        # Ensure list as data structure
        if isinstance(curves, list):
            pass
        else:
            curves = [curves]

        poly_list = []
        for i in range(1):  # Note, only takes the outer crv
            cur_crv = curves[i]
            coor_list = rs.CurvePoints(cur_crv)
            cur_polygon = []
            for pt in coor_list:
                cur_polygon.append([
                    utils.str_rounder(pt.X - refX, 2),
                    utils.str_rounder(pt.Y - refY, 2)
                ])
            if cur_polygon[0] != cur_polygon[-1]:
                cur_polygon.append(cur_polygon[0])
            poly_list.append([cur_polygon])
            
        building = {
            'buildingGeometry': {'type': 'Polygon', 'coordinates': poly_list},
            'buildingUse': buildingUse,
            'buildingHeight': height,
        }
        return ([curves, height], building) 

    def RunScript(self, B, U):
        result = None

        # Input parameter checking and defaults
        if B != []:
            self.B = B
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter B failed to collect data.'
            )
            return
        if U != []:
            self.U = U

        buildingUse_ref = {
            'residential': 0,
            'commercial': 0,
            'industrial': 0,
            'office': 0,
            'entertainment': 0, 
            'retail': 0, 
            'education': 0, 
            'mixed-use': 0
        }

        # set defaults for attributes
        len_b = len(self.B)
        if len(self.U) != len_b:
            self.U = self.U * len_b
        
        # structure data for threading 
        masterList = []
        for i in range(len_b):
            temp = {
                'mesh': self.B[i],
                'buildingUse': utils.check_attr(
                    self.U[i],
                    buildingUse_ref,
                    'residential'
                ),
                'projOrigin': (0, 0, 0)
            }
            masterList.append(temp)
        
        # run main function
        data = pr.run(self.mesh2json, masterList)
        result = {'json': [],'geometry': []}
        for obj in data:
            result['geometry'].append(obj[0])
            result['json'].append(obj[1])
        
        return [result]


class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):

    def get_AssemblyName(self):
        return 'CreateBuildings'

    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return '0.1'

    def get_AuthorName(self):
        return 'City Intelligence Lab'

    def get_Id(self):
        return System.Guid('1cf3aa8f-a031-40b9-bc53-e3abc9fa37d1')