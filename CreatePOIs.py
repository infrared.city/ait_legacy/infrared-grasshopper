# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import System
import ghpythonlib.parallel as pr

import InFraReDUtils as utils

class CreatePOIs(component):

    def __init__(self):
        super(CreatePOIs, self).__init__()

        # Defaults (None indicates required input params)
        self.P = None
        self.T = ['other']
        self.W = [1]

    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Create POIs',
            'IR Create POIs',
            'Use this component to turn points into InFraReD Points of Interest.',
            'InFraReD',
            '3 | Geometry'
        )
        return instance

    def get_ComponentGuid(self):
        return System.Guid('fe548a3d-6c19-4c37-964a-746a1a3c0ab6')

    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional

    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_Point()
        self.SetUpParam(p, 'POIs', 'P', 'A list of points representing points of interest.')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.list
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Type', 'T', 'A list with the same length as the input parameter POI (P) representing the type for each point of interest.', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.list
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_Number()
        self.SetUpParam(p, 'Weight', 'W', 'A list with the same legnth as the input parameter POI (P) representing the weight for each point of interest.', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.list
        self.Params.Input.Add(p)
    
    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'POIs', 'P', 'POI in the InFraReD format. Used as input in the "InFraReD Create Snapshot" component.')
        self.Params.Output.Add(p)
    
    def SolveInstance(self, DA):
        P = self.marshal.GetInput(DA, 0)
        T = self.marshal.GetInput(DA, 1)
        W = self.marshal.GetInput(DA, 2)
        result = self.RunScript(P, T, W)

        if result is not None:
            self.marshal.SetOutput(result, DA, 0, True)
        
    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAACxUlEQVRIid2V70tTURjHv9dMpyMY7A5XKM5KNAm3MirE4JpIIJkWQYSB12Cvp3+BSX+A9aY3N9g1kYigOVZBEbFACqPclEpHyx+ZZWQyM+eC2Y1zO+e23TZbEwn6wriM85zn83yf85x78d+I0xvhRKcJQOc2o9G+vLLSpcjS9EbNbtEBxKqdu66fbzndeqy2rnJrbm5n2MybOEfNMIIjsWwhHE3usPKW3uNHBKG6vCIpYHFpCV7/g0ggNE5cydlAcuiztdS6QyAJ9Vr8EkE8HjcZDYb2bJ0wCNqamlFcVISrnpsIz85g9VsMN+7dge/hfViMBhjz87NlIDfxz+6SUvXnf/YU/b5b2FNcjBLejPD8R1SVVwg1Ur8y82Fu+tXkG9K6wawgTMKBgwi8HIUhLw8Tc+/hOteBgnwDW7YN3PV5hkVnWaaTp7WLtEevr6sxHLbXJAJUnWpoJA9Xpk4YRL4oXfEPvxhLWox/X1PPSS8CtfIWR6YQtV3E9jJQPxCNCkOB5+4W4agtvraGdwufI2OvQyZyTokiUzi/8MmLn+NvI9MJgFziCClYkaVIYvxvN55u7ARgB9ADQGxrOtF9aG+1Brh2ezA4OTdbT5P3UgATAdQrshRcF5ICKlp5S/t2M49AaDxI4URTBNDd2qzF9gz6QEFlzFE6J+r7S5GlC+uBAbjb62oRiUbhHQmiZb8DpsJC9A09JiEn2ZjnpEuSgchZwMabMfp2Vo1eiq7CVFjAdmqDkRJCbdp1ldt0YWrPSdW9Z8+AtMzV2IC+oSds3a/t/UM7CFCmAA8AcvkuK7LkpzFTzJFQWQH/REgrQJGlfes6YcnJAXKi000mDICXJnQnhHVoZf8CEHUlFZwOksYdATxKfOVzotNDR5npkiJLSZC/PXhHim9KFx1Z0GdPin0bF7m8nOhU2DlumjjRKWwq4N8LwA82zQTsImzL8gAAAABJRU5ErkJggg=='
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )

    def point2json(self, master):
        # unpack master list
        pt = master['point']
        refX = master['projOrigin'][0]
        refY =  master['projOrigin'][1]

        # create JSON structure
        pt_coor = [
            utils.str_rounder(pt.X - refX, 2),
            utils.str_rounder(pt.Y - refY, 2)
        ]
        poiJson = {
            'poiClassification': master['type'],
            'poiCustomWeight': master['customWeight'],
            'poiGeometry': {
                'type': 'Point',
                'coordinates': pt_coor,
            }
        }
        return (pt, poiJson) 

    def RunScript(self, P, T, W):
        result = None

        # Input parameter checking and defaults
        if P != []:
            self.P = P
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter P failed to collect data.'
            )
            return
        if T != []:
            self.T = T
        if W != []:
            self.W = W

        # Definitions to process meshes
        poiType_ref = {
            'other': 0,
            'restaurant': 0,
            'school': 0,
            'transit': 0,
            'park': 0
        } 
        
        # set defaults for attributes
        len_p = len(self.P)
        if len(self.T) != len_p:
            self.T = self.T * len_p
        if len(self.W) != len_p:
            self.W = self.W * len_p
        
        # structure data for threading 
        masterList = []
        for i in range(len_p):
            temp = {
                'point': self.P[i],
                'projOrigin': (0, 0, 0),
                'type': utils.check_attr(self.T[i], poiType_ref, 'other'),
                'customWeight': self.W[i]
            }
            masterList.append(temp)
        
        # run main function
        data = pr.run(self.point2json, masterList)
        result = {'json': [], 'geometry': []}
        for obj in data:
            result['geometry'].append(obj[0])
            result['json'].append(obj[1])
        return [result]


class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):

    def get_AssemblyName(self):
        return 'CreatePOIs'

    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return '0.1'

    def get_AuthorName(self):
        return 'City Intelligence Lab'

    def get_Id(self):
        return System.Guid('3a4470f0-7130-4c21-a39a-349e36088ad7')