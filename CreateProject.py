# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import scriptcontext as sc
import System
import Rhino
import json
import uuid

import InFraReDUtils as utils

class CreateProject(component):

    def __init__(self):
        super(CreateProject, self).__init__()

        # Defaults (None indicates required input params)
        self.N = None
        self.D = None
        self.Sb = None
        self.Lat = 0
        self.Lng = 0
        self.A = None

    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Create Project',
            'IR Create Proj',
            'Use this component once at the start of your workflow to create a new blank project on the InFraReD server.',
            'InFraReD',
            '2 | Project'
        )
        return instance

    def get_ComponentGuid(self):
        return System.Guid('ac9f22ba-baf7-4b2c-a730-f7f85f043c2f')

    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional

    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Project Name', 'N', 'The name of your project.')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Project Description', 'D', 'A short description about your project.')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_Rectangle()
        self.SetUpParam(p, 'Site Boundary', 'Sb', 'A rect that represents the bounds of your project area.')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_Number()
        self.SetUpParam(p, 'South West Latitude', 'Lat', 'The latitude of the south-west corner of you site boundary. Defaults to Null Island.', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_Number()
        self.SetUpParam(p, 'South West Longitude', 'Lng', 'The longitude of the south-west corner of you site boundary. Defaults to Null Island.', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Analyses Settings', 'A', 'Generated by the `InFraReD Create Analysis Settings` component. Defaults to no analyses.')
        p.Access = Grasshopper.Kernel.GH_ParamAccess.list
        self.Params.Input.Add(p)

    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_Rectangle()
        self.SetUpParam(p, 'Site Boundary', 'Sb', 'A rectangle that represents the bounds of your project area.')
        self.Params.Output.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Project Metadata', 'm', 'Metadata of the created project in the InFraReD Schema format.')
        self.Params.Output.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Snapshots', 's', 'The names of all the snapshots in the project.')
        self.Params.Output.Add(p)

    def SolveInstance(self, DA):
        N = self.marshal.GetInput(DA, 0)
        D = self.marshal.GetInput(DA, 1)
        Sb = self.marshal.GetInput(DA, 2)
        Lat = self.marshal.GetInput(DA, 3)
        Lng = self.marshal.GetInput(DA, 4)
        A = self.marshal.GetInput(DA, 5)

        results = self.RunScript(N, D, Sb, Lat, Lng, A)
        if results is not None:
            self.marshal.SetOutput(results[0], DA, 0, True)
            self.marshal.SetOutput(results[1], DA, 1, True)
            self.marshal.SetOutput(results[2], DA, 2, True)

    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAABVUlEQVRIiWNkTEgVYGBgmM/AwACiqQ0O/l8wuwFkyXoGBoYAGlgAA45M1PBBf2Q4Q7yNFS5pBxZsogoiwgwKIiI4Df3w7RvDhUePwez6AF8GfyMDBgM5WbC+xg2bMdRjWAJSOCMxjmHn1es4LYmzNGc4cO0GQ+HylQwO6uoMAlycDAwiwgwODOoMjQxEWSICtuDik6c4LQHJ/fr/n2F+cgKDY2cPw/7yEoYDN29i9QVWS4gFO69dZ1AWFWGYHBMJ9hEoCHEBJnItCTI0YOBhZ2dgZGYBR/yDN2+pa8mi4ycZLj55Ambfff2a4ee/v3jVkx1cd1+/gbP1ZWXwqiU7uEgBo5aMWjJqySCxBKNYefDmDUOFrxdJhmhLSpBqyVuGjPmL8NaM6GD9qdOkWQKzCF/RTSL4AIqTg9QyDQv4wMDAsIERxGJMSE0A1bw0sGTB/wWzHwAAbORm1wVWlpAAAAAASUVORK5CYII='
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )

    def RunScript(self, N, D, Sb, Lat, Lng, A):
        results = None
        
        # Validate authentication
        auth_token = sc.sticky.get('InFraReDAuthToken')
        if not auth_token:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                'Invalid Credentials.\nPlease authenticate with the InFraReD server using the "InFraReD Authenticate" component.'
            )
            return
        
        # Grab stickies
        client_uuid = sc.sticky.get('InFraReDClientUuid')
        
        # Input parameter checking and defaults
        if N is not None:
            self.N = N
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter N failed to collect data.'
            )
            return
        if D is not None:
            self.D = D
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter D failed to collect data.'
            )
            return
        if Sb is not None:
            if Sb.Center.Z != 0.:
                component.AddRuntimeMessage(
                    self,
                    Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                    'Invalid site boundary (Sb).\nThe Z value of the site boundary origin must be 0.'
                )
                return
            elif Sb.Plane.Normal != Rhino.Geometry.Vector3d(0, 0, 1):
                component.AddRuntimeMessage(
                    self,
                    Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                    'Invalid site boundary (Sb).\nThe site boundary must be coplanar to WorldXY.'
                )
                return
            elif Sb.Height > 500. or Sb.Width > 500.:
                component.AddRuntimeMessage(
                    self,
                    Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                    'Invalid site boundary (Sb).\nThe site boundary is limited to a maximum of 500m per side.'
                )
                return
            elif Sb.Plane.XAxis != Rhino.Geometry.Vector3d(1, 0, 0) or \
                    Sb.Plane.YAxis != Rhino.Geometry.Vector3d(0, 1, 0):
                component.AddRuntimeMessage(
                    self,
                    Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                    'Invalid site boundary (Sb).\nThe site boundary must be oriented orthogonally to WorldXY.'
                )
                return
            else:
                self.Sb = Sb
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter Sb failed to collect data.'
            )
            return
        if Lat is not None:
            if Lat < -90 or Lat > 90:
                component.AddRuntimeMessage(
                    self,
                    Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                    'Invalid latitude (Lat).\nPlease enter a valid latitude.\nOr leave this parameter empty to have it default to Null Island.'
                )
                return
            else:
                self.Lat = Lat
        if Lng is not None:
            if Lng < -180 or Lng > 180:
                component.AddRuntimeMessage(
                    self,
                    Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                    'Invalid longitude (Lng).\nPlease enter a valid longitude.\nOr leave this parameter empty to have it default to Null Island.'
                )
                return
            else:
                self.Lng = Lng
        if A != []:
            errors = [False if a.type == 'Analysis' else True for a in A]
            if any(errors):
                component.AddRuntimeMessage(
                    self,
                    Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                    'Invalid Analyses (A).\nPlease enter a merged list of valid Analysis objects using the "InFraReD Analysis" and "Merge" components.'
                )
                return
            self.A = A
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter A failed to collect data.\nPlease input a collection of analyses using the "InFraReD Analysis" and "Merge" components.'
            )
            return
        
        # GQL request to get projects
        gql = System.Text.RegularExpressions.Regex.Replace("""
            {{ \" query \":
                \" query {{
                    getProjectsByUserUuid (
                        uuid: \\\" {} \\\"
                    ) {{
                        success,
                        infraredSchema
                    }}
                }} \"
            }}
            """,
            "\s+",
            ""
        ).format(
            client_uuid
        )
        
        # Send gql req
        out, auth_token = utils.send_gql_req_to_api(gql, auth_token)
        
        # Store server-created project uuid in a sticky
        return_data = json.loads(out)['data']['getProjectsByUserUuid']
        if return_data['success'] == True:
            projects = return_data['infraredSchema']['clients'][client_uuid]['projects']
        
        # Validate unique project name
        if N in [proj['projectName'] for proj in projects.values()]:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                'Invalid project name (N).\nThe project by this name already exists. Either enter a unique project name or use the "InFraReD Load Project" component.'
            )
            return
        
        # Infer SW most point
        O = Rhino.Geometry.Point3d(
            self.Sb.Center.X - self.Sb.Width/2,
            self.Sb.Center.Y - self.Sb.Height/2,
            0
        )
        
        # Infer site boundary
        Sb = Rhino.Geometry.Rectangle3d(
            Rhino.Geometry.Plane(
                O,
                Rhino.Geometry.Point3d(O.X + 1, O.Y, 0),
                Rhino.Geometry.Point3d(O.X, O.Y + 1, 0)
            ),
            self.Sb.Width,
            self.Sb.Height
        )
        
        # Prepare session settings
        session_settings = {
            'mode': 'context',
            'analysis': []
        }
        for i, a in enumerate(self.A):
            setting = a.analysis
            setting['uuid'] = str(uuid.uuid4())
            setting['index'] = str(i)
            session_settings['analysis'].append(setting)
        
        # GQL for create new blank project
        gql = System.Text.RegularExpressions.Regex.Replace("""
            {{ \" query \":
                \" mutation {{
                    createNewBlankProject (
                        name: \\\" {} \\\",
                        description: \\\" {} \\\",
                        sessionSettings: \\\" {} \\\",
                        analysisGridResolution: {},
                        analysisKpis: \\\" {{}} \\\",
                        analysisKpiGroups: \\\" {{}} \\\",
                        explorationLayout: \\\" {{}} \\\",
                        southWestLatitude: {},
                        southWestLongitude: {},
                        southWestX: {},
                        southWestY: {},
                        latitudeDeltaM: {},
                        longitudeDeltaM: {},
                        filters: \\\" {{}} \\\",
                        userUuid: \\\" {} \\\"
                    ) {{
                        success,
                        uuid
                    }}
                }} \"
            }}
            """,
            "\s+",
            ""
        ).format(
            self.N,
            self.D,
            json.dumps(session_settings).replace('"', '\\\\\\\"'),
            10,
            self.Lat,
            self.Lng,
            O.X,
            O.Y,
            self.Sb.Height,
            self.Sb.Width,
            client_uuid
        )
        
        # Send gql req
        out, auth_token = utils.send_gql_req_to_api(gql, auth_token)
        
        # Store server-created project uuid in a sticky
        return_data = json.loads(out)['data']['createNewBlankProject']
        if return_data['success'] == True:
            project_uuid = return_data['uuid']
            
        # GQL request to get server-generated metadata about project
        gql = System.Text.RegularExpressions.Regex.Replace("""
            {{ \" query \":
                \" query {{
                    getProjectByUuid (
                        uuid: \\\" {} \\\"
                    ) {{
                        success,
                        infraredSchema
                    }}
                }} \"
            }}
            """,
            "\s+",
            ""
        ).format(
            project_uuid
        )
        
        # Send gql req
        out, auth_token = utils.send_gql_req_to_api(gql, auth_token)
        
        # Store server-created project uuid in a sticky
        return_data = json.loads(out)['data']['getProjectByUuid']
        if return_data['success'] == True:
            m = json.dumps(return_data['infraredSchema']['clients'][client_uuid]['projects'], ensure_ascii=False)
        
        # GQL request to get server-generated origin snapshot from project creation
        gql = System.Text.RegularExpressions.Regex.Replace("""
            {{ \" query \":
                \" query {{
                    getSnapshotsByProjectUuid (
                        uuid: \\\" {} \\\"
                    ) {{
                        success,
                        infraredSchema
                    }}
                }} \"
            }}
            """,
            "\s+",
            ""
        ).format(
            project_uuid
        )
        
        # Send gql req
        out, auth_token = utils.send_gql_req_to_api(gql, auth_token)
        
        # Store server-created project uuid in a sticky
        return_data = json.loads(out)['data']['getSnapshotsByProjectUuid']
        if return_data['success'] == True:
            snapshots = return_data['infraredSchema']['clients'][client_uuid]['projects'][project_uuid]['snapshots']
            s = [x['snapshotName'] for x in snapshots.values()]

        # Update Auth token
        sc.sticky['InFraReDAuthToken'] = auth_token
        sc.sticky['InFraReDProjectUuid'] = project_uuid
        sc.sticky['InFraReDProjectList'] = projects
        sc.sticky['InFraReDSnapshotList'] = snapshots

        results = [Sb, m, s]
        return results


class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):

    def get_AssemblyName(self):
        return 'CreateProject'

    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return '0.1'

    def get_AuthorName(self):
        return 'City Intelligence Lab'

    def get_Id(self):
        return System.Guid('68667af7-94f1-48cb-b5cd-d8838ac90c66')
