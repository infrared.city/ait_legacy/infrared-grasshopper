# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import System

import InFraReDUtils as utils

class WindComfortAnalysisContainer(utils.Analysis):
    def __repr__(self):
        return '<InFraReD.WindComfortAnalysis instance at {:#018x}>'.format(
            id(self)
        )

class WindComfortAnalysis(component):

    def __init__(self):
        super(WindComfortAnalysis, self).__init__()

        # Defaults (None indicates required input params)
        self.S = 10
        self.D = 0
        self.v = {}
        self.f = {}

    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Wind Comfort Analysis',
            'IR Wind Comfort',
            "One of InFraReD's analysis modules. Merge multiple of these analysis components to define all the analyses ran on your project.",
            'InFraReD',
            '1 | Analysis'
        )
        return instance

    def get_ComponentGuid(self):
        return System.Guid("3ca46b88-05dc-4d6d-8af7-7fe1d5869daf")

    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional

    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_Number()
        self.SetUpParam(p, 'Wind Speed', 'S', 'Wind speed (m²) input argument for analysis module.', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_Number()
        self.SetUpParam(p, "Wind Direction", "D", "Wind direction (°) input argument for analysis module.", True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, "Visualisation", "v", "Currently not functioning. This input parameter is safe to leave empty.", True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, "Filters", "f", "Currently not functioning. This input parameter is safe to leave empty.", True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.item
        self.Params.Input.Add(p)

    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Analysis Container', 'A', 'A container for this analysis and the arguments.')
        self.Params.Output.Add(p)

    def SolveInstance(self, DA):
        S = self.marshal.GetInput(DA, 0)
        D = self.marshal.GetInput(DA, 1)
        v = self.marshal.GetInput(DA, 2)
        f = self.marshal.GetInput(DA, 3)

        result = self.RunScript(S, D, v, f)
        if result is not None:
            self.marshal.SetOutput(result, DA, 0, True)

    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAABp0lEQVRIid1Vy1HDMBB9YiggdAAVgCsgVECoAOWgM0kHoYIkZx8iOggVOFQAVBB3QDoQszNPzI4sx8aTU3ZGE3u13rf79hOcvxjrxnJOkajJKq37BHDN1zr4sjDWyfsSwIj6dfDldhAIo68AFAIAYA9gSoADgFcA9wBmAJ76AF2miuDLXQQn4IhZybkKvhSgrbFO9M/y3BvEWBdabDzPjgBaRvx2AeBWsgy+/Gqwk9DUV+5I35T2G/5K/W6O0VX9A+TAqL2xbtZlrEEeOmwlylorWBehZ6eaoiGarraaaJlK9AqgInVoowptc5I1tE5qMBFHGYAoRa7wFx2OfxTn32oQNwRIHb7k/BwFYesujXUVHXvqx6xDndhbyZjboR9I8OWcbfrBesx5FQuuna0ILJnvjXV/VDYmPgPkj1zrmrxLEMzCstv6gbRIwSz0jDxyKwiFC/1ZV02ywg6KVMWIZ21bYxCIihwcwBWfB3VXVox1E3bYgVt4TbvJSUA4iHEhyv6K66bmfTqggzKxsYWDL1dKHwezATKku4QeAUn/rN6oTwf0XATAL19XmK25ZlVHAAAAAElFTkSuQmCC'
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )

    def RunScript(self, S, D, v, f):
        result = None
        
        # Input parameter checking and defaults
        if S is not None:
            if S < 1 or S > 25:
                component.AddRuntimeMessage(
                    self,
                    Grasshopper.Kernel.GH_RuntimeMessageLevel.Error,
                    'Invalid wind speed (S).\nPlease enter a valid wind speed, between 1 and 25 ms.\nOr leave this parameter empty to have it default to 10m/s.'
                )
                return
            else:
                self.S = S
        if D is not None:
            self.D = D % 360
        if v is not None:
            # if TODO: validation
            #     component.AddRuntimeMessage(...
            # else:
            #     self.v = v
            pass
        if f is not None:
            # if TODO: validation
            #     component.AddRuntimeMessage(...
            # else:
            #     self.f = f
            pass
            
        # Compose session settings analysis
        result = WindComfortAnalysisContainer(
            {
                "name": "Wind Comfort",
                "parameters": [
                    {
                        "name": "Wind Speed",
                        "id": "windSpeed",
                        "tag": "slider",
                        "value": str(self.S)
                    },
                    {
                        "name": "Wind Direction",
                        "id": "windDirection",
                        "tag": "slider",
                        "value": str(self.D)
                    }
                ]
            }
        )
        
        return result


class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):

    def get_AssemblyName(self):
        return 'WindComfortAnalysis'

    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return '0.1'

    def get_AuthorName(self):
        return 'City Intelligence Lab'

    def get_Id(self):
        return System.Guid('3ac8301b-094d-4a29-8487-f23279cc0b8c')