# -*- coding: utf-8 -*-
from ghpythonlib.componentbase import dotnetcompiledcomponent as component
import Grasshopper, GhPython
import System
import rhinoscriptsyntax as rs
import ghpythonlib.components as ghcomp
import ghpythonlib.parallel as pr

import InFraReDUtils as utils

class CreateStreets(component):

    def __init__(self):
        super(CreateStreets, self).__init__()

        # Defaults (None indicates required input params)
        self.S = None
        self.T = ['footway']
        self.Lf = [1]
        self.Lb = [1]
        self.W = [1]

    def __new__(cls):
        instance = Grasshopper.Kernel.GH_Component.__new__(
            cls,
            'InFraReD Create Streets',
            'IR Create Streets',
            'Use this component to turn lines and curves into InFraReD streets.',
            'InFraReD',
            '3 | Geometry'
        )
        return instance
    
    def get_ComponentGuid(self):
        return System.Guid('7016d597-5fec-4159-9a4c-f8f1f8902d34')
    
    def SetUpParam(self, p, name, nickname, description, optional=False):
        p.Name = name
        p.NickName = nickname
        p.Description = description
        p.Optional = optional
    
    def RegisterInputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_Curve()
        self.SetUpParam(p, "Streets", "S", "A list of lines, polylines, or curves representing streets.")
        p.Access = Grasshopper.Kernel.GH_ParamAccess.list
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_String()
        self.SetUpParam(p, 'Type', 'T', 'A list with the same length as the input parameter Streets (S) representing the type for each street.', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.list
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_Number()
        self.SetUpParam(p, 'LanesForwards', 'Lf', 'A list with the same length as the input parameter Streets (S) representing the number of forward lanes for each street.', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.list
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_Number()
        self.SetUpParam(p, 'LanesBackwards', 'Lb', 'A list with the same length as the input parameter Streets (S) representing the number of backward lanes for each street.', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.list
        self.Params.Input.Add(p)
        
        p = Grasshopper.Kernel.Parameters.Param_Number()
        self.SetUpParam(p, 'Weight', 'W', 'A list with the same length as the input parameter Streets (S) representing the weight for each street.', True)
        p.Access = Grasshopper.Kernel.GH_ParamAccess.list
        self.Params.Input.Add(p)
    
    def RegisterOutputParams(self, pManager):
        p = Grasshopper.Kernel.Parameters.Param_GenericObject()
        self.SetUpParam(p, 'Streets', 'S', 'Streets in the InFraReD format. Used as input in the "InFraReD Create Snapshot" component.')
        self.Params.Output.Add(p)
    
    def SolveInstance(self, DA):
        S = self.marshal.GetInput(DA, 0)
        T = self.marshal.GetInput(DA, 1)
        Lf = self.marshal.GetInput(DA, 2)
        Lb = self.marshal.GetInput(DA, 3)
        W = self.marshal.GetInput(DA, 4)
        result = self.RunScript(S, T, Lf, Lb, W)

        if result is not None:
            self.marshal.SetOutput(result, DA, 0, True)
        
    def get_Internal_Icon_24x24(self):
        o = 'iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsSAAALEgHS3X78AAAC+UlEQVRIie1U30tTYRh+Tpu2SnBmbtoxN8Fphj/mD1SM3EEoSCpH1IUYdLxYdJf1D1g3dVdGBMGKHemiIsi8aZGQP2LCLjK1X6g3KlrNic0Stkm1eA/fWWdz2Vx30QOH833n+773Oc/7vO+HfwZcvBBOdOgBdPE5RvOC33c9IjkH/1asJo6gs6qktPfsiTbhYEPjXgCi32Q2h/eVj2NsNJAqCceCW015fG+r0Gwu2mOK2RAMh9A3+DwwMj56KSI5u1Mh2cLe9kKeN0/PzcpB1VhY9GHh0wf9Dp2uNVUlcro4a41w4VSHsF2ng9vzQl5I06bhwbMneDv1DmZDDpa/rgaCpWWTGBudSVWJDN5gRHvLUQTDYdy814MMDZCXlYXZpWU01TZY21uODVRfufqZEx3iZki0iT7Wl1Vg5KUXWo1GJjhzsg3btuqUNX2uZ9jlBmaSrbyokngvCKvBEBqsNVECBUJtHY2S9khR0n35zi1TY2W1eHh/U3Tx24/vyM7MXHeISKmP5tmcEx2Cej1eoZZ9DASADrfo6Hk1+b7ryAFBCK2tYX4pFPC+ntDHl7VccX7fECMwC7V1A2q1bqAqIjnH4pVE/+AjMHh7yU/G2gCcX1wZt/MGo4ulCMsrK3jY/5QCSOyYvb6sMialbs8wKUtMoiKTVEGkR0Cg3+s5nZu9Sz89N9tH30g9LfI5RhtVpdszLG+mdFsKTK1TQPeGJOz+6oxIzouM9PEXgJ51MO3mZT8sBb9SyhuMwpRq45YE55IGXUeFfD79EMg3xTt6c6LDviEJS0WM22Rwgq32CkuxPLhx/678qFTZklHSR7eyisDFiY4B9R9aCky2+B4CK/HyouJoWf+WhHygCuFEhwvAOQBDbOkaI9ZT7pX9ZD49CvKNuVbmbWLjVUTUVNHGYoQ9bCqo++d486GYsxWWEqo4Ui1t2nhW3gQbBVIgN+iiLzonVZkZGbY/KklA0KGMDTuzBaU3CN43E/KbLlcF6WnpQoIw/7EBAPwESiEG88JUBRIAAAAASUVORK5CYII='
        return System.Drawing.Bitmap(
            System.IO.MemoryStream(
                System.Convert.FromBase64String(o)
            )
        )

    def line2json(self, master):
        # unpack master list
        street = master['street']
        refX = 0  # master["projOrigin"][0]
        refY = 0  # master["projOrigin"][1]
        
        # siplify curves
        street = ghcomp.SimplifyCurve(street).curve
        
        # create JSON structure
        coor_list = rs.CurvePoints(street)
        street_coor = []
        for pt in coor_list:
            street_coor.append([
                utils.str_rounder(pt.X - refX, 2),
                utils.str_rounder(pt.Y - refY, 2)
            ])
            
        streetJson = {
            'streetSegmentClassification': master['type'],
            'streetSegmentForwardLanes': master['lanesForward'],
            'streetSegmentBackwardLanes': master['lanesBackward'],
            'streetSegmentCustomWeight': master['customWeight'],
            'streetSegmentGeometry': {
                'type': 'LineString',
                'coordinates': street_coor,
            }
        }

        return (street, streetJson) 

    def RunScript(self, S, T, Lf, Lb, W):
        result = None

        # Input parameter checking and defaults
        if S != []:
            self.S = S
        else:
            component.AddRuntimeMessage(
                self,
                Grasshopper.Kernel.GH_RuntimeMessageLevel.Warning,
                'Input parameter S failed to collect data.'
            )
            return
        if T != []:
            self.T = T
        if Lf != []:
            self.Lf = Lf
        if Lb != []:
            self.Lb = Lb
        if W != []:
            self.W = W

        streetType_ref = {
            'residential': 0,
            'footway': 0,
            'tertiary': 0,
            'secondary': 0,
            'primary': 0
        } 
        
        # set defaults for attributes
        len_s = len(self.S)
        if len(self.T) != len_s:
            self.T = self.T * len_s
        if len(self.Lf) != len_s:
            self.Lf = self.Lf * len_s
        if len(self.Lb) != len_s:
            self.Lb = self.Lb * len_s
        if len(self.W) != len_s:
            self.W = self.W * len_s
        
        # structure data for threading 
        masterList = []
        for i in range(len_s):
            temp = {
                'street': self.S[i],
                'projOrigin': (0, 0, 0),
                'type': utils.check_attr(self.T[i], streetType_ref, 'footway'),
                'lanesForward': self.Lf[i],
                'lanesBackward': self.Lb[i],
                'customWeight': self.W[i]
            }
            masterList.append(temp)
        
        # run main function
        data = pr.run(self.line2json, masterList)
        result = {'json': [], 'geometry': []}
        for obj in data:
            result['geometry'].append(obj[0])
            result['json'].append(obj[1])
        return [result]


class AssemblyInfo(GhPython.Assemblies.PythonAssemblyInfo):
    def get_AssemblyName(self):
        return 'CreateStreets'
    
    def get_AssemblyDescription(self):
        return ''

    def get_AssemblyVersion(self):
        return '0.1'

    def get_AuthorName(self):
        return 'City Intelligence Lab'
    
    def get_Id(self):
        return System.Guid('fe9575fa-6e6a-431c-b7a1-ad80cd7ccc31')